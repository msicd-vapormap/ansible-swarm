# Configuration d'un cluster Swarm avec Ansible à partir d'un poste local

__Par__: FETCHEPING FETCHEPING Rossif Borel  
__Email__: rossifetcheping@outlook.fr  
__École__: IMT Atlantique  
__Formation__: Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__: 2022-2023  
__UE__: Projet fil rouge  
__Dépôt Gitlab__: <https://gitlab.imt-atlantique.fr/vapormap-cicd/ansible-swarm>  
__Pages Gitlab__: <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/ansible-swarm>  
__Date__: 27 Février 2023  

## Prérequis

* Le déploiement de l'infrastructure du projet "terraform"
* Le fichier __public_ip.ini__ du répertoire __inventory/__ du projet "terraform" contenant les IP flottantes des instances  
* Le fichier __hosts.ini__ du répertoire __inventory/__ du projet "terraform" contenant l'inventaire des noeuds de l'infrastructure  
* La clé privée SSH de connexion aux instances  

## Préparer le système

* Mettre à jour le système

```sh
sudo apt update
```

* Installer les pré-requis : Environnement Python

```sh
sudo apt -y install python3 python3-pip python3-venv
```

## Configurer l'environnement de l'application

* Récupérer le dépôt  

```sh
cd $HOME
git clone git@gitlab.imt-atlantique.fr:vapormap-cicd/ansible-swarm.git vapormap/ansible-swarm
```

* Créer et activer l'environnement virtuel Python

```sh
cd $HOME/vapormap/ansible-swarm
python3 venv venv/ansible-swarm
source venv/ansible-swarm/bin/activate
```

* Installer les dépendances Python

```sh
pip install -r requirements/python-venv.txt
```

* Installer des collections Ansible

```sh
ansible-galaxy collection install -r requirements/ansible-collections.yml
```

* Copier l'inventaire généré par Terraform  

```bash
cd $HOME/vapormap/ansible-swarm
cp ../terraform/inventory/hosts.ini hosts.ini
```

* Configurer la connexion SSH aux noeuds: générer le fichier ssh.conf  

```sh
cd $HOME/vapormap/ansible-swarm

export BASTION_PUBLIC_IP='PUBLIC_IP_BASTION' # Valeur à remplacer
export BASTION_USER='ubuntu'
export NODE_USER='ubuntu'
export PRIVATE_KEY_PATH='keys/private_key.pem' # Valeur à remplacer

envsubst '$BASTION_PUBLIC_IP, $BASTION_USER, $NODE_USER, $PRIVATE_KEY_PATH' < ssh.conf.template > ssh.conf
```

> Remarque: Le répertoire __keys/__ est prévu pour garantir la sécurité de la clé si jamais vous souhaitez l'avoir à proximité dans le projet. Tous les fichiers de ce dossier sont systématiquement ignorés lors des commits.  

## Déployer Vapormap sur le cluster

* Lancer le Playbook de déploiement  

```sh
cd $HOME/vapormap/ansible-swarm
source venv/ansible-swarm/bin/activate

ansible-playbook deploy.yml
```

* Observer le resultat des tâches __Output__ dans le terminal  

## Supprimer le cluster

* Lancer le playbook de destroy  

```sh
ansible-playbook destroy.yml
```

* Observer le resultat des tâches __Output__ dans le terminal  

## Annexe : tests de qualité de code

* Évaluer la qualité de code du projet avec Ansible-lint et Yamllint  

```sh
cd $HOME/vapormap/ansible-swarm
source venv/ansible-swarm/bin/activate

make lint
```

* Consulter les fichiers de logs dans le répertoire lint/ qui va être généré  
