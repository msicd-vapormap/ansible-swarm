# Configuration d'un cluster Swarm avec Ansible

__Par__ : FETCHEPING FETCHEPING Rossif Borel  
__Email__ : rossifetcheping@outlook.fr  
__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet fil rouge  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/vapormap-cicd/ansible-swarm>  
__Pages Gitlab__ : <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/ansible-swarm>  
__Date__ : 27 Février 2023  

## Description

Ce projet permet de configurer et maintenir automatiquement un cluster Swarm sur une infrastructure d'instances Linux.  

* L'infrastruction du projet "terraform" du groupe "vapormap-cicd" doit être préalablement mise en place.  

* L'inventaire de l'infrastructure est généré par le projet "terraform"  

* Les opérations sont effectuées à partir des playbooks Ansible.  

* Les tâches à exécuter sont organisées dans des rôles.  

* La connexion SSH aux instances est configurée à l'aide du fichier __ssh.conf.template__ et des variables d'environnement. Le fichier __ssh.conf__ va être généré et utilisé par la configuration d'Ansible.  

## Utilisation du dépôt

Pour configurer le cluster Swarm, deux modes opératoires sont disponibles :  

* [Configuration depuis un poste local](./local.md)  
* [Configuration depuis un pipeline Gitlab CD](./pipeline.md)  

## Présentation de l'infrastructure

* L'infrastructure est constituée de trois noeuds contenus dans un réseau privé accessible en SSH à partir d'une instance bastion. Seul le noeud manager sera exploité pour ce déploiement.  
* Le noeud manager est rattaché à une IP flottante et expose des ports HTTP sur internet.  
* L'instance bastion est rattachée à une IP flottante et expose le port SSH sur internet pour permettre la configuration du cluster.  
