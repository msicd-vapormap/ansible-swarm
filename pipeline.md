# Configuration d'un cluster Swarm avec Ansible à partir d'un pipeline Gitlab

__Par__: FETCHEPING FETCHEPING Rossif Borel  
__Email__: rossifetcheping@outlook.fr  
__École__: IMT Atlantique  
__Formation__: Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__: 2022-2023  
__UE__ : Projet fil rouge  
__Dépôt Gitlab__: <https://gitlab.imt-atlantique.fr/vapormap-cicd/ansible-swarm>  
__Pages Gitlab__: <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/ansible-swarm>  
__Date__: 27 Février 2023  

## Prérequis

* La disponibilité de l'image Docker ansible-msicd:1.0 du projet "tools"  
* Le déploiement de l'infrastructure du projet "terraform"  
* Le package registry du projet "terraform":  
  * la disponibilité du fichier __public_ip.ini__ du package  
  * la disponibilité du fichier __hosts.ini__ du package  
* La clé privée SSH de connexion aux instances  

## Configurer l'environnement du pipeline sur Gitlab

### Créer ou mettre à jour les variables Gitlab du groupe "vapormap-cicd"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __PRIVATE_KEY__: contenu de la clé privée SSH (Utiliser une variable de type "File")  
  * __BASTION_PUBLIC_IP__: adresse IP publique de l'instance bastion (_Update: la variable est mise à jour automatiquement via le pipeline du projet Terraform_)  
  * __NODE_PUBLIC_IP__: adresse IP publique du noeud manager (_Update: la variable est mise à jour automatiquement via le pipeline du projet Terraform_)  
  * __BASTION_USER__: utilisateur SSH bastion  
  * __NODE_USER__: utilisateur SSH des noeuds  
  * __TF_PROJECT_ID__: identifiant du projet Terraform  
  * __TF_PKG_NAME__: nom du package registry des fichiers d'inventaire générés par Terraform.  
  * __TF_PKG_VERSION__: version du package registry   

## Vérifier l'inventaire

Il faut s'assurer que le fichier d'inventaire __hosts.ini__ de l'infrastructure est disponible dans le package registry __vapormap_pkg_tf__ du projet "terraform".  

## Configurer le cluster Swarm

* Se rendre dans l'onglet "CI/CD > pipeline" du projet.  
* Cliquer sur le bouton __"Run pipeline"__.  
* Dans la fénètre qui s'affiche, cliquer à nouveau sur le bouton __"Run pipeline"__ pour valider l'opération sans renseigner de variables.  
* Observer le resultat des tâches __Output__ dans le terminal du Job __deploy_swarm_cluster__  

## Supprimer le cluster  

* Se rendre dans le pipeline précédent  
* Déclencher manuellement le job __destroy_swarm_cluster__  
* Observer le resultat des tâches __Output__ dans le terminal du Job  
